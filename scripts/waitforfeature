#!/usr/bin/env python

from __future__ import print_function
import cv2, os, datetime, sys
from argparse import ArgumentParser
from time import sleep
import json
from analyze_image import analyze_image

p = ArgumentParser()
p.add_argument("--count", type=int, default=2, help="number of consecutive frames with features to require")
p.add_argument("--camera", type=int, default=0, help="camera number, default: 0")
p.add_argument("--cascades", default="./haarcascades", help="location of the cascade XML files, default: ./haarcascades")
p.add_argument("--settings", default="settings.json", help="settings file, default settings.json")
args = p.parse_args()

with open (args.settings) as f:
    settings = json.load(f)

cascades_path = os.path.expanduser(args.cascades)

def init_features (f):
    if type(f) == list:
        return [init_features(x) for x in f]
    f.get("cascade")
    f['_cascade'] = cv2.CascadeClassifier(os.path.join(cascades_path, f["cascade"]))
    if 'args' not in f:
        f['args'] = {}
    return f

def ensure_list (f):
    if type(f) == list:
        return f
    else:
        return [f]

def tuple_lists(args):
    for (key, value) in args.items():
        if type(value) == list:
            args[key] = tuple(value)
    return args

def process_features (grayimg, features):
    ret = {}
    for x in features:
        roi = grayimg
        ox, oy = 0, 0
        if type(x) == list:
            f1, f2 = x
        else:
            f1 = x
            f2 = None


        if f1['name'] not in ret:
            rects = ret[f1['name']] = []
        else:
            rects = ret[f1['name']]
        if f2:
            if f2['name'] not in ret:
                rects2 = ret[f2['name']] = []
            else:
                rects2 = ret[f2['name']]

        for (x,y,w,h) in f1['_cascade'].detectMultiScale(roi, **tuple_lists(f1['args'])):
            rects.append([int(x), int(y), int(w), int(h)])
            # narrow for (evt) next cascaded feature...
            if f2:
                roi = roi[y:y+h, x:x+w]
                for (x2,y2,w2,h2) in f2['_cascade'].detectMultiScale(roi, **tuple_lists(f2['args'])):
                    rects2.append([int(x+x2), int(y+y2), int(w2), int(h2)])
    return ret

cap = cv2.VideoCapture(args.camera)
count = 0
features = init_features(settings['features'])

while(True):
    ret, img = cap.read()
    if img == None:
        print ("ERROR CAPTURING FRAME. CHECK CAMERA CONNECTION AND SETTINGS", file=sys.stderr)
        sys.exit(0)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    item_features = process_features(gray, features)
    frame_feature_count = 0
    for key in item_features.keys():
        frame_feature_count += len(item_features[key])

    if frame_feature_count > 0:
        count += 1
        if count >= args.count:
            print (count)
            break
cap.release()


