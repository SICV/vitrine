#!/usr/bin/env python
from __future__ import print_function
from argparse import ArgumentParser
from PIL import Image
import cv2, os, numpy, sys

p = ArgumentParser("cat frames together, optional SRT formatted output")
p.add_argument("input", nargs="+", default=[])
p.add_argument("--output", default=None, help="path to save movie, default: None (show live)")
p.add_argument("--fourcc", default="XVID", help="MJPG,mp4v,XVID")
p.add_argument("--framerate", type=float, default=25, help="output frame rate")
p.add_argument("--resize", default=False, action="store_true")
p.add_argument("--pad", default=False, action="store_true")
p.add_argument("--width", type=int, default=640, help="pre-detect resize width")
p.add_argument("--height", type=int, default=480, help="pre-detect resize height")
p.add_argument("--background", default="255,255,255")
p.add_argument("--srt", default=False, action="store_true")
args = p.parse_args()


def resize (img, w, h, interpolation = cv2.INTER_CUBIC):
    ih, iw, ic = img.shape
    if (ih > h) or (iw > w):
        # try fitting width
        sw = w
        sh = int(sw * (float(ih)/iw))
        if sh > h:
            # fit height instead
            sh = h
            sw = int(sh * (float(iw)/ih))
        return cv2.resize(img, (sw, sh), interpolation=interpolation)
    return img

def pad (img, w, h, color=(0, 0, 0)):
    ih, iw, ic = img.shape
    top = (h - ih) / 2
    bottom = h - ih - top
    left = (w - iw) / 2
    right = (w - iw - left)
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)

def process_frame (frame):
    global out, framecount
    if args.resize:
        frame = resize(frame, args.width, args.height)
    if not args.pad:
        frame = pad(frame, args.width, args.height, background)
    framecount += 1
    if args.output:
        # VIDEO OUTPUT
        if out == None:
            out = cv2.VideoWriter()
            out.open(args.output, fourcc, args.framerate, (args.width, args.height))
        out.write(frame)
    else:
        # WINDOW DISPLAY
        cv2.imshow('display', frame)
        if cv2.waitKey(delay) & 0xFF == ord('q'):
            raise Exception("stop")

def processGIF(path):
    frame = Image.open(path)
    nframes = 0
    while frame:
        # process frame
        try:
            nf = numpy.array(frame.convert("RGB"))
            # RGB => BGR
            nf = nf[:, :, ::-1]
            process_frame(nf)
            nframes += 1
        except IOError:
            break
        try:
            frame.seek( nframes )
        except EOFError:
            break
    return nframes

def processVideo (path):
    cap = cv2.VideoCapture(path)
    count = 0
    while cap.isOpened():
        ret, frame = cap.read()
        if frame == None:
            cap.release()
            return count
        count += 1
        process_frame(frame)

if args.srt:
    from srt import timecode
delay = int(1.0 / args.framerate * 1000)

background = tuple([int(x) for x in args.background.split(",")])
fourcc = None
if args.output:
    try:
        fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
    except AttributeError:
        fourcc = cv2.VideoWriter_fourcc(*args.fourcc)
framecount = 0
out = None
files = args.input
files.sort()
for f in files:
    print (f, file=sys.stderr)
    if args.srt:
        print ("{0} -->\n{1}\n".format(timecode(float(framecount)/args.framerate), f))
    b, ext = os.path.splitext(f)
    ext = ext.lstrip(".").lower()
    if ext == "gif":
        processGIF (f)
    else:
        processVideo(f)
if out:
    out.release()
else:
    cv2.destroyAllWindows()

print ("Processed {0} frames".format(framecount), file=sys.stderr)
