import cv2


def analyze_image (img, face_cascade, eye_cascade, scaleFactor=None, minNeighbors=None, minSize=None):
    ret = False
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    
    args = dict()
    if scaleFactor:
        args['scaleFactor'] = scaleFactor
    if minNeighbors:
        args['minNeighbors'] = minNeighbors
    if minSize:
        args['minSize'] = (minSize, minSize)

    # face_cascade.detectMultiScale(**args)
    faces = face_cascade.detectMultiScale(gray, **args)
    # if scaleFactor !=None and minNeighbors != None:
    #     faces = face_cascade.detectMultiScale(gray, scaleFactor, minNeighbors, minSize=(300, 300))
    # elif scaleFactor != None:
    #     faces = face_cascade.detectMultiScale(gray, scaleFactor)
    # else:
    #     faces = face_cascade.detectMultiScale(gray)
    ar_faces=[]
    ar_eyes=[]
    for (x,y,w,h) in faces:
        #print "face", (x, y, w, h)
        ar_faces.append([int(x), int(y), int(w), int(h)])
        #print type(x)
        ret = True
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        #print img
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
            aex=int(ex)+int(x)
            aey=int(ey)+int(y)
            ar_eyes.append([aex,aey,int(ew),int(eh)]);
            #print 'eye'
    return ret, ar_faces, ar_eyes
