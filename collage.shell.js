var io = require('socket.io-client'),
	fs = require('fs'),
	spawn = require('child_process').spawn,
	player = null;
var settings = JSON.parse(fs.readFileSync("settings.json", "utf8")),
	serverbase = "http://"+settings.ipaddress+":"+settings.port,
	socket = io.connect(serverbase);

socket.on("connect", function (data) {
	console.log("connected to socket server");
});
socket.on("collage", function (d) {
	// video.style.display = "none";
	if (player) {
		player.kill();
		player = null;
	}
	// player = spawn('cvlc', ['--no-video-title-show', '--loop', 'collage/creatingcollage.mp4']);
	player = spawn('scripts/play', ['collage/creatingcollage.mp4']);
	console.log("Creating collage...");
});
socket.on("collagegif", function (d) {
	console.log("collagegif", d.src);
	if (player !== null) {
		player.kill();
		player = null;
	}
	console.log("scripts/playurl " + (serverbase+"/"+d.src));
	player = spawn('scripts/playurl', [serverbase+"/"+d.src]);
});
