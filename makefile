clean:
	rm -f output/*
	rm -f frames/*

settings.json:
	node server_init.js > settings.json

archive.json: archive/
	./getfaces.py archive/* > archive.json


creatingcollage.mp4: creatingcollage.png
	ffmpeg -framerate 1/10 -i creatingcollage.png -c:v libx264 -r 15 -pix_fmt yuv420p creatingcollage.mp4