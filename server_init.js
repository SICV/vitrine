
var settings = {port: 10987};

/* IP Address */
console.error("Checking IP address");
var os = require('os');
var ifaces = os.networkInterfaces();
var ret;

Object.keys(ifaces).forEach(function (ifname) {
var alias = 0;

ifaces[ifname].forEach(function (iface) {
  if ('IPv4' !== iface.family || iface.internal !== false) {
    // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
    return;
  }

  if (alias >= 1) {
    // this single interface has multiple ipv4 addresses
    console.error(ifname + ':' + alias, iface.address);
    settings.ipaddress = iface.address;
  } else {
    // this interface has only one ipv4 adress
    console.error("Using", ifname, iface.address);
    settings.ipaddress = iface.address;
  }
  ++alias;
});
});



console.log(JSON.stringify(settings));
