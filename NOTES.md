todo
-----
* add metadata to webm's with full provenance of images / links


machines/install
--------
* 10.9.8.7: robotnik/server/camera.html
* 10.9.8.3: archive.html (previously search.html)
* 10.9.8.2: collage.html

Generalised Features
------------------------
* settings ...
{ 'features': [
		[
			{'name': 'faces', 'cascade': 'haarcascade_frontalface_default.xml', 'color': '255,255,0', 'width': 2},
			{'name': 'eyes', 'cascade': haarcascade_frontalface_default.xml', 'color': '255,255,0', 'width': 2}
		],
		{'name': 'bodies', 'cascade': haarcascade_frontalface_default.xml', 'color': '255,255,0', 'width': 2}
	]
}
* scripts/getfaces => getfeatures
* collect all feature groups (in hierarchy)
* cascadecollage.js
	facecount => featurecount
	process_frame & draw_faces
* archive: generalize to iterators for all named feature groups
* scripts/waitforface => waitforfeature
