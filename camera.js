document.addEventListener("DOMContentLoaded", function () {
	window.requestAnimationFrame = window.webkitRequestAnimationFrame || window.requestAnimationFrame;

	var sockaddr = window.location.protocol+'//'+window.location.host,
		socket,
		img = document.querySelector("img"),
		video = document.querySelector("video"),
		msg = document.getElementById("message"),
		cameragif,
		framecounter = 0;

	img.style.display = "none";
	console.log("opening connection to", sockaddr);
	socket = io.connect();
	socket.on("connect", function (data) {
		console.log("connected to socket server");
	});
	socket.on("watch", function () {
		// start a countdown...
		imagedata = null;
		msg.innerHTML = "Waiting for a face...";
		img.style.display = "none";
		video.pause();
		video.style.display = "none";
		requestAnimationFrame(draw);
	});
	socket.on("countdown", function () {
		// start a countdown...
		imagedata = null;
		msg.innerHTML = "About to record...";
		img.style.display = "none";
		requestAnimationFrame(draw);
	})
	socket.on("record", function () {
		// start a countdown...
		imagedata = null;
		msg.innerHTML = "Recording...";
		img.style.display = "none";
		requestAnimationFrame(draw);
	})
	socket.on("analyze", function () {
		// start a countdown...
		msg.innerHTML = "Searching for faces <br>from the camera...";
		framecounter = 0;
		img.style.display = "none";
	})
	socket.on("match", function () {
		// start a countdown...
		msg.innerHTML = "Searching for faces...";
		img.style.display = "none";
	})
	socket.on("archiveframe", function (d) {
		// img.style.display = "block";
		// img.src = d.cf.path;
	})
	socket.on("cameraimage", function (d) {
		// console.log("cameraimage", d.src);
		// var img = new Image();
		// imagesByPath[d.src] = img;
		// img.src = d.src;
		// img.style.display = "block";
		// img.src = d.src;
	});
	function s (f) {
		return f.length == 1 ? "" : "s";
	}
	
	socket.on("imagedata", function (d) {
		var m;
		// img.src = d.path;
		// img.style.display = "block";
		framecounter++;

		if (!d.faces || d.faces.length == 0) {
			msg.innerHTML = "Frame "+framecounter+": No faces detected";
		} else {
			msg.innerHTML = "Frame "+framecounter+": " + d.faces.length+" face"+s(d.faces)+" + "+d.eyes.length+" eye"+s(d.eyes)+" detected"
		}
		// imagedata = d;
		// requestAnimationFrame(draw);
	})
	socket.on("annotatedcameraframe", function (d) {
		img.src = d.src;
		video.style.display = "none";
		img.style.display = "block";
	});
	socket.on("collage", function (d) {
		msg.innerHTML = "Creating collage...";
		img.style.display = "none";
		video.style.display = "none";
	})
	socket.on("cameragif", function (d) {
		// msg.innerHTML = "Creating collage...";
		cameragif = d.src;
		// video.src = d.src;
		img.style.display = "none";
		// video.style.display = "block";
	});
	socket.on("collagegif", function (d) {
		msg.innerHTML = "";
		video.src = cameragif;
		video.style.display = "block";
	});
});

