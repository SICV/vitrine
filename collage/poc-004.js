$( document ).ready(function() {
    var items=[];
    var faces=[];
    var eyes=[];
    var eyes_names=[];
    var faces_names=[];
    var faces_images_names=[];
    var eyes_images_names=[];
    var cntr=0;
    var curr=0;
    var lpp;

    //loading archive in json format
    $.getJSON( "jindex.json", function( data ) {
	$.each( data, function( key, val ) {
	    for(var i=0;i<val.faces.length;i++){
		faces.push(val.faces[i]);
		faces_names.push(val.faces_names[i]);
		faces_images_names.push(val.name);
	    }
	    for(var j=0;j<val.eyes.length;j++){
		eyes.push(val.eyes[j]);
		eyes_names.push(val.eyes_names[j]);
		eyes_images_names.push(val.name);

	    }
	});
    });

    //loading info from camera and search
    $.getJSON( "test-webcam-multi.json", function( data ) {
	$.each( data, function( key, val ) {
	    var im='<div id="image"  style="position:relative;"><img src="'+val.path[0].replace("../poc/testing","testing")+'" /></div>';
	    $('#input').append(im);
	    items=val;
	    setUpScreen();
	});
    });

    function setUpScreen(){
	console.log(items);
	cntr=0;
	$('#screen_left').html(" ");
	for(var i=0;i<items.faces.length;i++){
	    var idx=randPick(faces.length);
	    var repl_face=faces_names[idx];
	    var pickFace_str='<div class="pick_left_'+cntr+'"  style="display:none;position:absolute;left:'+items.faces[i][0]+'px;top:'+items.faces[i][1]+'px;width:'+items.faces[i][2]+'px;height:'+items.faces[i][3]+'px;"><img src="testing/crops/'+repl_face+'" style="width:'+items.faces[i][2]+'px;height:'+items.faces[i][3]+'px;" /></div>';
	    $('#image').append(pickFace_str);
	    var replFace_str='<div class="pick_image_'+cntr+'"  style="display:none;position:absolute;left:0px;top:0px;"><img src="'+faces_images_names[idx].replace("/home/nicolas/Pictures/Guttormsgaard/testing","testing")+'" /></div>';
	    $('#screen_left').append(replFace_str);
	    cntr++;
	}
	for(var i=0;i<items.eyes.length;i++){
	    var idx=randPick(eyes.length);
	    var repl_eye=eyes_names[idx];
	    var pickEye_str='<div class="pick_left_'+cntr+'"  style="display:none;position:absolute;left:'+items.eyes[i][0]+'px;top:'+items.eyes[i][1]+'px;width:'+items.eyes[i][2]+'px;height:'+items.eyes[i][3]+'px;"><img src="testing/crops/'+repl_eye+'" style="width:'+items.eyes[i][2]+'px;height:'+items.eyes[i][3]+'px;" /></div>';
	    $('#image').append(pickEye_str);
	    var replEye_str='<div class="pick_image_'+cntr+'"  style="display:none;position:absolute;left:0px;top:0px;"><img src="'+eyes_images_names[idx].replace("/home/nicolas/Pictures/Guttormsgaard/testing","testing")+'" /></div>';
	    $('#screen_left').append(replEye_str);
	    cntr++;

	}
	startScan();
    }

    function startScan() {
	lpp = setInterval(scan, 3000);
    }

    function scan() {
	console.log("entering scan");
	var tgt_left=".pick_left_"+curr;
	var tgt_image=".pick_image_"+curr;
	$(tgt_left).show();
	$(tgt_image).show();
	if(curr<cntr){
	    curr++;
	}else{
	    clearInterval(lpp);
	    curr=0;
	    setUpScreen();
}
    }

    function randPick(l){
	var pick=Math.floor((Math.random() * (l-1)) + 1);
	return pick;
    }
    //startScan();
});

