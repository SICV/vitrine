document.addEventListener("DOMContentLoaded", function () {
	var sockaddr = window.location.protocol+'//'+window.location.host,
		socket,
		images = [],
		imagedata = [],
		eyes = [],
		canvas = document.getElementById("canvas"),
		ctx = canvas.getContext("2d"),
		canvas_rect,
		image2screen;

	var STATE_WATCHING = 0,
		STATE_RECORDING = 1,
		state = STATE_WATCHING;

	// patch requestAnimationFrame
	window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame;

	console.log("opening connection to", sockaddr);
	socket = io.connect();
	socket.on("connect", function (data) {
		console.log("connected to socket server");
	});

	document.getElementById("watch").addEventListener("click", watch);

	function load_image (path) {
		var img = new Image();
		images.push(img);
		img.addEventListener("load", function () {
			redraw();
		})
		img.src = path;
	}

	function watch () {
		images = []; imagedata = [];
		socket.emit("recordframe");
	}

	socket.on("cameraimage", function (data) {
		console.log("cameraimage", data);
		load_image(data.image);
	});
	socket.on("imagedata", function (data) {
		data = data[0];
		imagedata.push(data);
		console.log("imagedata", data, data.faces ? data.faces.length : 0);
		if (data.faces) {
			// START RECORDING...
			// state = STATE_RECORDING;
			// socket.emit("record");
			console.log("found faces", data.faces.length);
			redraw();
		} else {
			console.log("no faces")
			// NO FACE... look again in a bit...
		}
		window.setTimeout(watch, 1000);
	})

	function redraw () {
		requestAnimationFrame(draw);
	}

	function draw () {
		// console.log("draw");
		var image = images ? images[0] : undefined,
			data = imagedata ? imagedata[0] : undefined;
		if (image) {
			var im_rect = cc.rect(0, 0, image.width, image.height);
			if (image2screen == undefined) {
				var r = cc.frame_rect(canvas_rect, im_rect, true);
				image2screen = cc.transform_from(im_rect, r);
			}
			var sr = image2screen(im_rect);
			ctx.drawImage(image, sr.x, sr.y, sr.width, sr.height);
			// ctx.drawImage(image, 0, 0);
			if (data && data.faces) {
				ctx.strokeStyle = "blue";
				ctx.lineWidth = 3;
				data.faces.forEach(function (x) {
					var r = image2screen(cc.rect(x[0], x[1], x[2], x[3]));
					console.log("drawFaces", x, r);
					ctx.strokeRect(r.x, r.y, r.width, r.height);
				});
			}
			if (data && data.eyes) {
				ctx.strokeStyle = "green";
				ctx.lineWidth = 3;
				data.eyes.forEach(function (x) {
					var r = image2screen(cc.rect(x[0], x[1], x[2], x[3]));
					ctx.strokeRect(r.x, r.y, r.width, r.height);
				})				
			}
		}
	}

	function resize () {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		canvas_rect = cc.rect(0, 0, canvas.width, canvas.height);
		image2screen = undefined;
		redraw();
	}
	resize();
	window.addEventListener("resize", resize)
	window.requestAnimationFrame(draw);
	window.setTimeout(watch, 1000);

});
