
var spawn = require("child_process").spawn;

var cmd = spawn("convert", ["test.jpg", "-fill", "none", "-stroke", "blue", "-strokewidth", "2", "-draw", "rectangle 10,10 20,20", "drawtest.png"]);

cmd.stderr.setEncoding('utf8');
cmd.stderr.on("data", function (data) {
	console.log("err", data);
});

cmd.on("close", function (code) {
	console.log("convert finished", code);
});


