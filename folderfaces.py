import numpy as np
import cv2, os, json
from time import sleep

#tpath = os.path.expanduser("~/opencv/opencv-3.0.0/data/haarcascades")
tpath = os.path.expanduser("~/opencv/data/haarcascades")
face_cascade = cv2.CascadeClassifier(os.path.join(tpath, 'haarcascade_frontalface_default.xml'))
eye_cascade = cv2.CascadeClassifier(os.path.join(tpath, 'haarcascade_eye.xml'))

from argparse import ArgumentParser
import thread
from numpy import zeros, array

class pict:

    def __init__(self, name):
        self.name = name
        self.faces = []    # creates a new empty list of faces
        self.eyes=[]    # creates a new empty list of eyes
        self.faces_names = []    # creates a new empty list of face names
        self.eyes_names=[]    # creates a new empty list of eye names

    def add_faces(self, facelist):
        self.faces=facelist

    def add_eyes(self, eyelist):
        self.eyes=eyelist

    def add_faces_names(self, facenamelist):
        self.faces_names=facenamelist

    def add_eyes_names(self, eyenamelist):
        self.eyes_names=eyenamelist

    def serialize(self):
        ser={"faces":self.faces,"faces_names":self.faces_names, "eyes":self.eyes, "eyes_names":self.eyes_names, "name":self.name}
        return ser

p = ArgumentParser("")
p.add_argument("input", nargs="+", default=[])
p.add_argument("-d","--destdir", nargs="?", default="/home/nicolas/code/vitrine/jsons")
args = p.parse_args()
jindex=[]

def process_image (img,iname):
    ret = False
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    ar_faces=[]
    ar_faces_names=[]
    ar_eyes=[]
    ar_eyes_names=[]
    cnt=0;
    for (x,y,w,h) in faces:
        print "face", (x, y, w, h)
        ar_faces.append([int(x), int(y), int(w), int(h)])
        #print type(x)
        ret = True
        #cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        #print img
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        roi_color_name=iname+"_roi_"+str(cnt)+'.jpg'
        ar_faces_names.append(roi_color_name)
        cv2.imwrite(roi_color_name,roi_color)
        eyes = eye_cascade.detectMultiScale(roi_gray)
        cnt_eye=0
        for (ex,ey,ew,eh) in eyes:
            #cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
            #absolute coordinates for eyes
            aex=int(ex)+int(x)
            aey=int(ey)+int(y)
            aew=int(ew)
            aeh=int(eh)
            ar_eyes.append([aex,aey,aew,aeh]);
            eye_color=roi_color[ey:ey+eh,ex:ex+ew]
            eye_color_name=iname+"_eye_"+str(cnt)+'_'+str(cnt_eye)+'.jpg'
            cv2.imwrite(eye_color_name,eye_color)
            ar_eyes_names.append(eye_color_name);
            cnt_eye+=1
        cnt+=1
    return img, ret, ar_faces, ar_faces_names, ar_eyes, ar_eyes_names

for i in args.input:
    print i
    img = cv2.imread(i)
    img, detection, faces, faces_names, eyes, eyes_names = process_image(img,os.path.basename(i))
    if detection:
        pc=pict(i)
        pc.add_faces(faces)
        pc.add_faces_names(faces_names)
        pc.add_eyes(eyes)
        pc.add_eyes_names(eyes_names)
        dest_file=args.destdir+"/"+os.path.basename(i)+".json"
        sr=pc.serialize()
        print sr
        with open(dest_file, 'w') as outfile:
            json.dump(sr, outfile)
#        print p.faces
#        cv2.imshow('img', img)
#        if cv2.waitKey(500) & 0xFF == ord('q'):
#            break
        jindex.append(sr)
    #cv2.imwrite("output.jpg", img)

# cv2.imwrite("output.jpg", frame)
with open(args.destdir+"/"+'jindex.json','w') as jifile:
    json.dump(jindex,jifile)
#cv2.destroyAllWindows()


