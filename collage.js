document.addEventListener("DOMContentLoaded", function () {
	var sockaddr = window.location.protocol+'//'+window.location.host,
		socket,
		img = document.querySelector("img"),
		video = document.querySelector("video"),
		msg = document.getElementById("message");

	img.style.display = "none";
	video.style.display = "none";
	console.log("opening connection to", sockaddr);
	socket = io.connect();
	socket.on("connect", function (data) {
		console.log("connected to socket server");
	});
	socket.on("analyze", function (data) {
		console.log("connected to socket server");
		// img.style.display = "none";
	});
	socket.on("collage", function (d) {
		video.style.display = "none";
		msg.innerHTML = "Creating collage...";
	});
	socket.on("collagegif", function (d) {
		console.log("collagegif", d.src);
		msg.innerHTML = "";
		video.src = d.src;
		video.style.display = "block";
	});
})
