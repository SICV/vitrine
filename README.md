SICV Vitrine
============

Code for the SICV installation in the window of Constant, Brussels, February-March 2016.

Requirements
-----------------
* node + packages
* python + pyopencv

The server is written in javascript and requires node to run it. Camera and feature detection uses python and opencv (via the pyopencv bindings).

For node, the required packages can be installed with the command:

    apt-get install nodejs npm python-opencv
    npm install socket.io node-static javascript-state-machine extend dateformat async minimist socket.io-client

and install ffmpeg via deb-multimedia:

	# /etc/apt/sources.list
	# AND ADD THE SOURCE...

	deb http://www.deb-multimedia.org jessie main non-free

and then:

	wget https://www.deb-multimedia.org/pool/main/d/deb-multimedia-keyring/deb-multimedia-keyring_2015.6.1_all.deb
	sudo dpkg -i deb-multimedia-keyring_2015.6.1_all.deb
	sudo apt-get update
	sudo apg-get install ffmpeg



Preparing archive material
-----------------------------

Use getfaces to make a json data file of features for a collection of images.


	scripts/getfaces erkki/*.jpg > erkki.json
	scripts/getfaces asger/*.jpg > asger.json
	scripts/getfaces archive/*.jpg > archive.json


Combining JSON files into a single one
-----------------------------------------

	scripts/jsoncat erkki.json asger.json archive.json > archive.all.json


Starting the server
------------------------

When you start the server, you can specify options such as the archive file with:

	node server.js --archive=archive.all.json

or

	node server.js --archive=archive.all.json --port 8000


Viewing the screens
------------------------

Open three separate browsers to:

	http://localhost:10987/camera.html
	http://localhost:10987/archive.html
	http://localhost:10987/collage.html

or from different machines to:

	http://10.9.8.7:10987/camera.html
	http://10.9.8.7:10987/archive.html
	http://10.9.8.7:10987/collage.html

using whatever IP address (in place of 10.9.8.7) the server has.

